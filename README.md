# "machi-pla" Development Environment

## workflow

This dokumentation describes the development workflow in machi-pla. How to get started with your
local development environment and how to contribute to machi-pla projects.

## source code and version control
All machi-pla projects are hosted on [Bitbucket](https://bitbucket.org/) and are version controlled
with git. In case you do not have an account on [Bitbucket](https://bitbucket.org/) please register.
After you created your please request access to the source code in [Slack](https://machi-pla.slack.com/messages/C0LBQV5U1/).
### setup git on your local machine

Git should be installed on every mac (for windows please download [here](https://git-for-windows.github.io))
